import { Test, TestingModule } from '@nestjs/testing';
import { TasksController } from './tasks.controller';
import { TasksService } from './tasks.service';

describe('TasksController', () => {
  let controller: TasksController;
  let service: TasksService;

  const task = { content: 'Test task', done: false };
  const patched_task = { content: 'Test task', done: true };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TasksController],
      providers: [TasksService],
    }).compile();

    service = module.get<TasksService>(TasksService);
    controller = module.get<TasksController>(TasksController);
  });

  it('controller should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('service should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('task post test', () => {
    it('should add new task to array', async () => {
      expect(controller.createTask({ content: 'Test tasks' })).toMatchObject(
        task,
      );
      expect(controller.getTasks()).toMatchObject([task]);
    });
  });

  describe('task patch test', () => {
    it('should patch task', async () => {
      const new_task = controller.createTask({ content: 'Test task' });
      expect(controller.updateTask(new_task.id, { done: true })).toMatchObject(
        patched_task,
      );
      expect(controller.getTasks()).toMatchObject([patched_task]);
    });
  });

  describe('task delete test', () => {
    it('should delete task from array', async () => {
      const new_task = controller.createTask({ content: 'Test task' });
      expect(controller.removeTask(new_task.id)).toMatchObject(task);
      expect(controller.getTasks()).toHaveLength(0);
    });
  });
});
