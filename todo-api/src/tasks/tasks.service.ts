import { Injectable } from '@nestjs/common';
import { UpdateTaskDto } from './dto/update-task.dto';
import { CreateTaskDto } from './dto/create-task.dto';
@Injectable()
export class TasksService {
  private tasks = [];

  getTasks(onlyDone?: 'true' | 'false') {
    if (!onlyDone) return this.tasks;

    if (onlyDone === 'true')
      return this.tasks.filter((task) => task.done === true);

    return this.tasks.filter((task) => task.done === false);
  }

  getTask(id: number) {
    const task = this.tasks.find((task) => task.id === id);

    if (!task) {
      throw new Error(`task with id ${id} not found`);
    }

    return task;
  }

  createTask(createTaskDto: CreateTaskDto) {
    const newTask = {
      ...createTaskDto,
      done: false,
      id: Date.now(),
    };
    this.tasks.unshift(newTask);

    return newTask;
  }

  updateTask(id: number, updateTaskDto: UpdateTaskDto) {
    this.tasks = this.tasks.map((task) => {
      if (task.id === id) {
        return { ...task, ...updateTaskDto };
      }
      return task;
    });

    return this.getTask(id);
  }

  removeTask(id: number) {
    const toBeRemoved = this.getTask(id);

    this.tasks = this.tasks.filter((task) => task.id !== id);

    return toBeRemoved;
  }
}
