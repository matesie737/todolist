import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { TasksService } from './tasks.service';
@Controller('tasks')
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  // GET /tasks - wyświetlanie wszystkich zadań.
  @Get()
  getTasks(@Query('done') done?: 'true' | 'false') {
    return this.tasksService.getTasks(done);
  }

  // POST /tasks - dodawanie nowego zadania.
  @Post()
  createTask(@Body() createTaskDto: CreateTaskDto) {
    return this.tasksService.createTask(createTaskDto);
  }

  // DELETE /tasks/:id - usuwanie zadania.
  @Delete(':id')
  removeTask(@Param('id', ParseIntPipe) id: number) {
    try {
      return this.tasksService.removeTask(id);
    } catch (err) {
      throw new NotFoundException();
    }
  }

  // PATCH /tasks/:id - aktualizacja zadania
  @Patch(':id')
  updateTask(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateTaskDto: UpdateTaskDto,
  ) {
    return this.tasksService.updateTask(id, updateTaskDto);
  }
}
