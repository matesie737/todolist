import { MinLength } from "class-validator";

export class CreateTaskDto {
    @MinLength(5)
    content: string
}
