import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"
import { Tasks, TaskPayload, TaskUpdatePayload, ToggleType } from "types"

export const tasksApi = createApi({
  reducerPath: "tasks",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:8000/tasks",
  }),
  tagTypes: ["tasks"],
  endpoints: (builder) => ({
    getTasks: builder.query({
      query: (data: ToggleType) => ({
        url: `${data.done && data.done !== "all" ? `?done=${data.done}` : ""}`,
        method: "GET",
      }),
      providesTags: ["tasks"],
      transformResponse: (response: Tasks) => response,
    }),
    postTask: builder.mutation({
      query: (taskPayload: TaskPayload) => ({
        url: `/`,
        method: "POST",
        body: taskPayload,
      }),
      invalidatesTags: ["tasks"],
    }),
    patchTask: builder.mutation({
      query: (params: {
        id: number
        taskUpdatePayload: TaskUpdatePayload
      }) => ({
        url: `/${params.id}`,
        method: "PATCH",
        body: params.taskUpdatePayload,
      }),
      invalidatesTags: ["tasks"],
    }),
    deleteTask: builder.mutation({
      query: (id: number) => ({
        url: `/${id}`,
        method: "DELETE",
      }),
      invalidatesTags: ["tasks"],
    }),
  }),
})

export const {
  useGetTasksQuery,
  usePostTaskMutation,
  usePatchTaskMutation,
  useDeleteTaskMutation,
} = tasksApi
