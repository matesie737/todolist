export type Tasks = Task[]

export interface Task {
  id: number
  content: string
  done: boolean
}
export interface TaskUpdatePayload {
  done: boolean
}

export interface TaskPayload {
  content: string
}

export interface ToggleType {
  done?: "true" | "false" | "all"
}
