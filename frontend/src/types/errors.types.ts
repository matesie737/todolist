interface ErrorData {
  error: string
  message: string[]
  statusCode: number
}

export interface ApiError {
  data?: ErrorData
  status: number
}
export interface ApiResponse {
    data?: any,
    error?: ApiError | unknown
}