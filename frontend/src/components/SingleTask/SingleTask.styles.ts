import styled from "@emotion/styled"

export const Container = styled("div")({
  width: "100%",
  display: "flex",
  flexDirection: "row",
})

export const Text = styled("p")({
  width: "100%",
  fontSize: "24px",
})

export const Image = styled("img")({
  height: "22px",
})
