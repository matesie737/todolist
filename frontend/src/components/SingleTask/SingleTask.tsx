import { Task } from "types"
import { Container, Text, Image } from "./SingleTask.styles"
import { done, done_grey, not_done } from "utils"
import { Button, Tooltip } from "@mui/material"

const SingleTask = (props: {
  data: Task
  handlePatch: (id: number, done: boolean) => void
  handleDelete: (id: number) => void
}) => {
  const { data, handlePatch, handleDelete } = props

  return (
    <Container>
      <Tooltip title={data.done ? "mark undone" : "mark done"}>
        <Button
          onClick={() => {
            handlePatch(data.id, data.done)
          }}
          size="small"
          data-testid="patch_button"
        >
          <Image src={data.done ? done : done_grey} />
        </Button>
      </Tooltip>
      <Text
        style={{ textDecoration: data.done ? "line-through" : "none" }}
        data-testid="task_content"
      >
        {data.content}
      </Text>
      <Tooltip title="delete task">
        <Button
          onClick={() => {
            handleDelete(data.id)
          }}
          size="small"
          data-testid="delete_button"
        >
          <Image src={not_done} />
        </Button>
      </Tooltip>
    </Container>
  )
}

export default SingleTask
