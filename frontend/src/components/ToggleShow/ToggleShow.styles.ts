import styled from "@emotion/styled"
import { ToggleButtonGroup as BaseToggleButtonGroup } from "@mui/material"

export const ToggleButtonGroup = styled(BaseToggleButtonGroup)({
  height: "30px",
})

export const Image = styled("img")({
  height: "20px",
})
