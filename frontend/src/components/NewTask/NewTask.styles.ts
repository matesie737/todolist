import styled from "@emotion/styled"
import { Button as BaseButton, TextField as BaseTextField } from "@mui/material"

export const Container = styled("form")({
  display: "flex",
  flexDirection: "row",
  width: "100%",
})

export const Button = styled(BaseButton)({
    
  fontFamily: "'Delicious Handrawn', cursive",
  fontSize: "30px",
  fontWeight: "bold",
  color: "#000000",
  padding: "0px",
  margin: "-10px",
})

export const TextField = styled(BaseTextField)({
  margin: "0 15px",
})

export const TextFieldInput = {
  style: {
    fontSize: "20px",
    fontFamily: "'Delicious Handrawn', cursive",
  },
}
