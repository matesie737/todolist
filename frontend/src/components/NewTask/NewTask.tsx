import { useState } from "react"
import { Button, Container, TextField, TextFieldInput } from "./NewTask.styles"
import { usePostTaskMutation } from "api"
import { Tooltip } from "@mui/material"
import { ApiResponse } from "types"

const NewTask = () => {
  const [addTask] = usePostTaskMutation()
  const [taskContent, setTaskContent] = useState<string>("")
  const [errorState, setErrorState] = useState<boolean>(false)
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTaskContent(event.target.value)
  }
  const clear = () => {
    setErrorState(false)
    setTaskContent("")
  }
  const handleSubmit = () => {
    addTask({ content: taskContent }).then((response: ApiResponse) => {
      response.data ? clear() : setErrorState(true)
    })
  }
  return (
    <Container>
      <Tooltip title="add task">
        <Button onClick={handleSubmit} data-testid="new_task_button">
          +
        </Button>
      </Tooltip>
      <TextField
        data-testid="new_task_textfield"
        fullWidth
        size="small"
        onSubmit={handleSubmit}
        InputProps={TextFieldInput}
        value={taskContent}
        onChange={handleChange}
        error={errorState}
        helperText={errorState ? "Task must contain at least 5 letters" : ""}
        onKeyDown={(event) => {
          setErrorState(false)
          if (event.key === "Enter") {
            handleSubmit()
            event.preventDefault()
          }
        }}
      />
      <Tooltip title="clear">
        <Button onClick={clear} data-taskid="clear_button">-</Button>
      </Tooltip>
    </Container>
  )
}

export default NewTask
