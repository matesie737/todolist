import { SingleTask } from "components"
import { Task, Tasks } from "types"
import { Container } from "./TaskList.styles"

const TaskList = (props: {
  data?: Tasks
  handlePatch: (id: number, done: boolean) => void
  handleDelete: (id: number) => void
}) => {
  const { data, handlePatch, handleDelete } = props
  return (
    <Container>
      {data?.map((task: Task) => {
        return (
          <SingleTask
            key={task.id}
            data={task}
            handlePatch={handlePatch}
            handleDelete={handleDelete}
          />
        )
      })}
    </Container>
  )
}

export default TaskList
