import styled from "@emotion/styled"

export const Container = styled("div")({
  display: "flex",
  flexDirection: "column",
  gap: "5px",
  marginTop: "10px",
  overflow: "auto",
})
