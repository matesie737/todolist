import { TaskList } from "components/TasksList"
import { Container, Box, Title, TopBar } from "./Tasks.styles"
import { NewTask, ToggleShow } from "components"
import { useState, useEffect } from "react"
import { ToggleType } from "types"
import {
  useDeleteTaskMutation,
  useGetTasksQuery,
  usePatchTaskMutation,
} from "api"

const Tasks = () => {
  const [toggleState, setToggleState] = useState<ToggleType>({ done: "all" })
  const handleSetToggleState = (type: ToggleType) => {
    setToggleState(type)
  }
  const { data, refetch } = useGetTasksQuery(toggleState)
  useEffect(() => {
    refetch()
  }, [toggleState, refetch])

  const [deleteTask] = useDeleteTaskMutation()
  const [patchTask] = usePatchTaskMutation()

  const handlePatch = (id: number, done: boolean) => {
    patchTask({ id: id, taskUpdatePayload: { done: !done } })
  }
  const handleDelete = (id: number) => {
    deleteTask(id)
  }

  return (
    <Container>
      <Box>
        <TopBar>
          <Title>TO DO LIST!</Title>
          <ToggleShow
            setToggleState={handleSetToggleState}
            toggleState={toggleState}
          />
        </TopBar>
        <NewTask />
        <TaskList
          data={data}
          handlePatch={handlePatch}
          handleDelete={handleDelete}
        />
      </Box>
      Mateusz Siedalczek
    </Container>
  )
}

export default Tasks
