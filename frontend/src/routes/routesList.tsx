import { Tasks } from "modules"
import { Navigate } from "react-router-dom"

export const routesList = [
  {
    path: "/tasks",
    element: <Tasks />
  },
  {
    path: "/",
    element: <Navigate to="/tasks" replace />,
  },
  {
    path: "*",
    element: <>not found</>,
  },
]
